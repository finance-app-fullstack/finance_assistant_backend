import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from .config import settings


def send_mail(email: str, subject: str, message: str):
    if settings.mail_debug:
        print(f'send message to {email}')
        print(f'subject {subject}')
        print(f'message {message}')
    else:
        msg = MIMEMultipart()

        password = settings.smtp_password
        msg['From'] = settings.smtp_from
        msg['To'] = email
        msg['Subject'] = subject

        msg.attach(MIMEText(message, 'plain'))

        server = smtplib.SMTP(host=settings.smtp_server, port=settings.smtp_port)

        server.starttls()

        server.login(msg['From'], password)

        server.sendmail(msg['From'], msg['To'], msg.as_string())

        server.quit()
