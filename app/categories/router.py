from typing import List

from fastapi import APIRouter, Depends
from fastapi.responses import Response
from sqlalchemy.orm import Session

from app import crud
from app.dependencies import db_session, get_user
from app.models import User, Operation

from .schemas import CategoryInfo, CategoryCreate, CategoryInfoWithOperationsCount, Category

router = APIRouter()


@router.get('/', response_model=List[CategoryInfo])
def get_user_categories(db: Session = Depends(db_session), user: User = Depends(get_user)):
    return crud.get_user_categories(db, user.id)


@router.post('/', response_model=CategoryInfo, status_code=201)
def create_category(category_data: CategoryCreate, db: Session = Depends(db_session), user: User = Depends(get_user)):
    category_data.user_id = user.id
    return crud.create_category(db, category_data)


@router.get('/{category_id}/', response_model=CategoryInfoWithOperationsCount)
def category_detail(category_id: int, db: Session = Depends(db_session), user: User = Depends(get_user)):
    category = crud.get_user_category(db, user.id, category_id)
    category_schema = CategoryInfoWithOperationsCount.from_orm(category)
    category_schema.operations_count = category.operations.filter(
        Operation.deleted.is_(False)
    ).count()
    return category_schema.dict()


@router.put('/{category_id}/', response_model=CategoryInfo)
def category_update(
        updated_data: Category,
        category_id: int,
        db: Session = Depends(db_session),
        user: User = Depends(get_user)
):
    return crud.update_category(db, user.id, category_id, updated_data.name)


@router.delete('/{category_id}/')
def remove_category(category_id: int, db: Session = Depends(db_session), user: User = Depends(get_user)):
    crud.remove_category(db, user.id, category_id)
    return Response(status_code=204)
