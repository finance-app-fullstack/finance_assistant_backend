import datetime
from typing import Optional

from pydantic import BaseModel, Field


class Category(BaseModel):
    name: str = Field(max_length=50, min_length=1)


class CategoryCreate(Category):
    user_id: Optional[int]


class CategoryInfo(Category):
    id: int
    created: datetime.datetime

    class Config:
        orm_mode = True


class CategoryInfoWithOperationsCount(Category):
    id: int
    created: datetime.datetime
    operations_count: Optional[int]

    class Config:
        orm_mode = True

