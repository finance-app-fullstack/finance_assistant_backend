PATH = '/categories/'


def test_ok(client, user):
    response = client.post(
        PATH,
        headers={'Authorization': f'Token {user.token}'},
        json={'name': 'Категория'}
    )
    assert response.status_code == 201

    category_info = response.json()
    assert category_info['name'] == 'Категория'
    assert category_info['id'] == 1


def test_min_length_name(client, user):
    response = client.post(
        PATH,
        headers={'Authorization': f'Token {user.token}'},
        json={'name': ''}
    )

    assert response.status_code == 400
    assert response.json() == {'error_details': {'name': ['Не может быть меньше 1 символов']}}


def test_name_max_value(client, user):
    response = client.post(
        PATH,
        headers={'Authorization': f'Token {user.token}'},
        json={'name': 'r' * 51}
    )

    assert response.status_code == 400
    assert response.json() == {'error_details': {'name': ['Не может быть больше 50 символов']}}


def test_empty_body(client, user):
    response = client.post(
        PATH,
        headers={'Authorization': f'Token {user.token}'},
        json={}
    )

    assert response.status_code == 400
    assert response.json() == {'error_details': {'name': ['Поле не может быть пустым']}}


def test_user_not_exist(client, db):
    response = client.post(
        PATH,
        headers={'Authorization': 'Token 123'},
        json={'name': 'Категория'}
    )

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}


def test_without_token(client):
    response = client.post(
        PATH,
        json={'name': 'Категория'}
    )

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}
