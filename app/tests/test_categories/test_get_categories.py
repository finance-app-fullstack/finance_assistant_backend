from app.tests.factories import category_factory, user_factory, operation_factory

PATH = '/categories/'


def test_get_categories(client, user, db):
    category_one = category_factory(db, user, 'Категория_1')
    category_two = category_factory(db, user, 'Категория_2')

    response = client.get(PATH, headers={'Authorization': f'Token {user.token}'})

    resp_cat_one = response.json()[0]
    resp_cat_two = response.json()[1]

    assert response.status_code == 200

    assert resp_cat_one['id'] == category_two.id
    assert resp_cat_one['name'] == category_two.name
    assert resp_cat_one['created'] == category_two.created.isoformat()

    assert resp_cat_two['id'] == category_one.id
    assert resp_cat_two['name'] == category_one.name
    assert resp_cat_two['created'] == category_one.created.isoformat()


def test_only_auth_user_category(client, user, db):
    user_2 = user_factory(db, email='em@em.com', password='123')

    category_one = category_factory(db, user, 'Категория_1')
    category_factory(db, user_2, 'Категория_2')

    response = client.get(PATH, headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 200
    assert len(response.json()) == 1

    response_category = response.json()[0]
    assert response_category['id'] == category_one.id
    assert response_category['name'] == category_one.name
    assert response_category['created'] == category_one.created.isoformat()


def test_only_active_categories(client, user, db):
    category_one = category_factory(db, user, 'Категория_1')
    category_factory(db, user, 'Категория_2', deleted=True)

    response = client.get(PATH, headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 200
    assert len(response.json()) == 1

    response_category = response.json()[0]
    assert response_category['id'] == category_one.id
    assert response_category['name'] == category_one.name
    assert response_category['created'] == category_one.created.isoformat()


def test_without_categories(client, user):
    response = client.get(PATH, headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 200
    assert len(response.json()) == 0


def test_get_categories_user_not_exist(client, db):
    response = client.get(PATH, headers={'Authorization': 'Token 123'})

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}


def test_get_category(client, user, db):
    category = category_factory(db, user, 'Категория')

    response = client.get(f'{PATH}{category.id}/', headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 200

    resp_cat = response.json()
    assert resp_cat['id'] == category.id
    assert resp_cat['name'] == category.name
    assert resp_cat['created'] == category.created.isoformat()
    assert resp_cat['operations_count'] == 0


def test_get_category_with_operations(client, user, db):
    category = category_factory(db, user, 'Категория')

    operation_factory(db, category)
    operation_factory(db, category)
    operation_factory(db, category)
    operation_factory(db, category)

    response = client.get(f'{PATH}{category.id}/', headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 200

    resp_cat = response.json()
    assert resp_cat['id'] == category.id
    assert resp_cat['name'] == category.name
    assert resp_cat['created'] == category.created.isoformat()
    assert resp_cat['operations_count'] == 4


def test_get_category_with_only_active_operations(client, user, db):
    category = category_factory(db, user, 'Категория')

    operation_factory(db, category)
    operation_factory(db, category)
    operation_factory(db, category, deleted=True)
    operation_factory(db, category, deleted=True)

    response = client.get(f'{PATH}{category.id}/', headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 200

    resp_cat = response.json()
    assert resp_cat['id'] == category.id
    assert resp_cat['name'] == category.name
    assert resp_cat['created'] == category.created.isoformat()
    assert resp_cat['operations_count'] == 2


def test_get_only_active_category(client, user, db):
    category = category_factory(db, user, 'Категория', deleted=True)

    response = client.get(f'{PATH}{category.id}/', headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 400
    assert response.json() == {'error_detail': 'Категория не найдена.'}


def test_get_category_not_exist(client, user):
    response = client.get(f'{PATH}20/', headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 400
    assert response.json() == {'error_detail': 'Категория не найдена.'}


def test_get_category_user_not_exist(client, db):
    response = client.get(f'{PATH}20/', headers={'Authorization': 'Token 123'})

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}


def test_get_category_without_token(client):
    response = client.get(f'{PATH}20/')

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}
