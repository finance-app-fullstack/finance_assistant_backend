from app.tests.factories import category_factory, user_factory

PATH = '/categories'


def test_ok(client, user, db):
    category = category_factory(db, user, 'Категория')
    response = client.put(
        f'{PATH}/{category.id}/',
        headers={'Authorization': f'Token {user.token}'},
        json={'name': 'Новая категория'}
    )

    assert response.status_code == 200

    category_response = response.json()
    assert category_response['name'] == 'Новая категория'
    assert category_response['id'] == category.id
    assert category_response['created'] == category.created.isoformat()

    db.refresh(category)
    assert category.name == 'Новая категория'


def test_deleted_category(client, user, db):
    category = category_factory(db, user, 'Категория', deleted=True)

    response = client.put(
        f'{PATH}/{category.id}/',
        headers={'Authorization': f'Token {user.token}'},
        json={'name': 'Категория'}
    )

    assert response.status_code == 400
    assert response.json() == {'error_detail': 'Категория не найдена.'}

    db.refresh(category)
    assert category.deleted


def test_category_other_user(client, user, db):
    category_one = category_factory(db, user, 'Категория_1')

    user_2 = user_factory(db, email='em@em.com', password='123')
    category_factory(db, user_2, 'Категория_2')

    response = client.put(
        f'{PATH}/{category_one.id}/',
        headers={'Authorization': f'Token {user_2.token}'},
        json={'name': 'Категория'}
    )

    assert response.status_code == 400
    assert response.json() == {'error_detail': 'Категория не найдена.'}

    db.refresh(category_one)
    assert category_one.name == 'Категория_1'


def test_category_not_exist(client, user, db):
    category = category_factory(db, user, 'Категория')

    response = client.put(
        f'{PATH}/123/',
        headers={'Authorization': f'Token {user.token}'},
        json={'name': 'Новая категория'}
    )

    assert response.status_code == 400
    assert response.json() == {'error_detail': 'Категория не найдена.'}

    db.refresh(category)
    assert category.name == 'Категория'


def test_min_length_name(client, user, db):
    category = category_factory(db, user, 'Категория')

    response = client.put(
        f'{PATH}/{category.id}/',
        headers={'Authorization': f'Token {user.token}'},
        json={'name': ''}
    )

    assert response.status_code == 400
    assert response.json() == {'error_details': {'name': ['Не может быть меньше 1 символов']}}

    db.refresh(category)
    assert category.name == 'Категория'


def test_name_max_value(client, user, db):
    category = category_factory(db, user, 'Категория')

    response = client.put(
        f'{PATH}/{category.id}/',
        headers={'Authorization': f'Token {user.token}'},
        json={'name': 'r' * 51}
    )

    assert response.status_code == 400
    assert response.json() == {'error_details': {'name': ['Не может быть больше 50 символов']}}

    db.refresh(category)
    assert category.name == 'Категория'


def test_empty_body(client, user, db):
    category = category_factory(db, user, 'Категория')

    response = client.put(
        f'{PATH}/{category.id}/',
        headers={'Authorization': f'Token {user.token}'},
        json={}
    )

    assert response.status_code == 400
    assert response.json() == {'error_details': {'name': ['Поле не может быть пустым']}}

    db.refresh(category)
    assert category.name == 'Категория'


def test_user_not_exist(client, user, db):
    category = category_factory(db, user, 'Категория')

    response = client.put(
        f'{PATH}/{category.id}/',
        headers={'Authorization': 'Token 123'},
        json={'name': 'Категория'}
    )

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}

    db.refresh(category)
    assert category.name == 'Категория'


def test_without_token(client, user, db):
    category = category_factory(db, user, 'Категория')

    response = client.put(
        f'{PATH}/{category.id}/',
        json={'name': 'Категория'}
    )

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}

    db.refresh(category)
    assert category.name == 'Категория'
