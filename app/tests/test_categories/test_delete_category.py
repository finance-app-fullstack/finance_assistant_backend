from app.tests.factories import category_factory, user_factory, operation_factory

PATH = '/categories'


def test_ok(client, user, db):
    category = category_factory(db, user, 'Категория')

    response = client.delete(
        f'{PATH}/{category.id}/',
        headers={'Authorization': f'Token {user.token}'}
    )

    assert response.status_code == 204

    db.refresh(category)
    assert category.deleted


def test_delete_related_operations(client, user, db):
    category = category_factory(db, user, 'Категория')

    operation_factory(db, category)
    operation_factory(db, category)
    operation_factory(db, category)

    response = client.delete(
        f'{PATH}/{category.id}/',
        headers={'Authorization': f'Token {user.token}'}
    )

    assert response.status_code == 204

    db.refresh(category)
    assert category.deleted

    for operation in category.operations.all():
        assert operation.deleted


def test_category_other_user(client, user, db):
    category_one = category_factory(db, user, 'Категория_1')

    user_2 = user_factory(db, email='em@em.com', password='123')
    category_factory(db, user_2, 'Категория_2')

    response = client.delete(
        f'{PATH}/{category_one.id}/',
        headers={'Authorization': f'Token {user_2.token}'},
        json={'name': 'Категория'}
    )

    assert response.status_code == 400
    assert response.json() == {'error_detail': 'Категория не найдена.'}

    db.refresh(category_one)
    assert not category_one.deleted


def test_category_not_exist(client, user, db):
    category = category_factory(db, user, 'Категория')

    response = client.delete(
        f'{PATH}/123/',
        headers={'Authorization': f'Token {user.token}'},
        json={'name': 'Новая категория'}
    )

    assert response.status_code == 400
    assert response.json() == {'error_detail': 'Категория не найдена.'}

    db.refresh(category)
    assert not category.deleted


def test_user_not_exist(client, user, db):
    category = category_factory(db, user, 'Категория')

    response = client.delete(
        f'{PATH}/{category.id}/',
        headers={'Authorization': 'Token 123'},
        json={'name': 'Категория'}
    )

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}

    db.refresh(category)
    assert not category.deleted


def test_without_token(client, user, db):
    category = category_factory(db, user, 'Категория')

    response = client.delete(
        f'{PATH}/{category.id}/',
        json={'name': 'Категория'}
    )

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}

    db.refresh(category)
    assert not category.deleted
