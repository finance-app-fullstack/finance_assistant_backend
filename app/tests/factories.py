import random

from app.models import (
    ResetPasswordPin,
    User,
    OperationCategory,
    Operation,
)


def user_factory(db, email, password):
    db_user = User(
        email=email,
        hashed_password=User.get_hash_password(password),
        token=User.generate_token()
    )

    db.add(db_user)
    db.commit()
    return db_user


def reset_pin_code_factory(db, **data):
    if 'pin' not in data:
        data['pin'] = random.randint(1000, 9999)
    pin = ResetPasswordPin(**data)
    db.add(pin)
    db.commit()
    return pin


def category_factory(db, user, name=None, **data):
    data['user_id'] = user.id
    if name:
        data['name'] = name
    else:
        data['name'] = random.choice(['Авто', 'Продукты', 'Здоровье'])
    category = OperationCategory(**data)
    db.add(category)
    db.commit()
    return category


def operation_factory(db, category, **data):
    if 'name' not in data:
        data['name'] = random.choice(['Мойка', 'Продукты', 'Лекарства', 'Заправка'])
    if 'amount' not in data:
        data['amount'] = random.randint(500, 1000000)
    data['user_id'] = category.user_id
    data['category_id'] = category.id
    operation = Operation(**data)
    db.add(operation)
    db.commit()
    return operation
