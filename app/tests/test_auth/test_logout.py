PATH = '/user/logout/'


def test_success(client, user, db):
    data = {'email': user.email, 'password': 'password'}
    old_token = user.token
    response = client.post(PATH, json=data, headers={'Authorization': f'Token {user.token}'})

    db.refresh(user)

    assert response.status_code == 200
    assert old_token != user.token


def test_invalid_token(client, user, db):
    data = {'email': user.email, 'password': 'password'}
    old_token = user.token
    response = client.post(PATH, json=data, headers={'Authorization': f'Token 123'})

    db.refresh(user)

    assert response.status_code == 400
    assert response.json() == {'error_detail': 'Не удалось найти пользователя.'}
    assert old_token == user.token


def test_without_auth_header(client, user, db):
    data = {'email': user.email, 'password': 'password'}
    old_token = user.token
    response = client.post(PATH, json=data)

    db.refresh(user)

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}
    assert old_token == user.token
