import pytest

PATH = '/user/login/'


def test_success(client, user):
    data = {'email': user.email, 'password': 'password'}

    response = client.post(PATH, json=data)

    assert response.status_code == 200
    assert response.json() == {
        'token': user.token,
    }


def test_wrong_password(client, user):
    data = {'email': user.email, 'password': 'password1'}

    response = client.post(PATH, json=data)

    assert response.status_code == 400
    assert response.json() == {
        'error_detail': 'Неправильный email или пароль',
    }


def test_user_not_exist(client, db):
    data = {'email': 'user@useremail.com', 'password': 'password'}

    response = client.post(PATH, json=data)

    assert response.status_code == 400
    assert response.json() == {
        'error_detail': 'Неправильный email или пароль',
    }


@pytest.mark.parametrize('email', [
    'user@',
    '@email.com',
    'user@email#email.com',
    'user@^tt.com',
    'user@user'
])
def test_invalid_email(client, email):
    data = {'email': email, 'password': 'password'}

    response = client.post(PATH, json=data)

    assert response.status_code == 400
    assert response.json() == {'error_details': {'email': ['Email введен неправильно']}}


def test_without_body(client):
    response = client.post(PATH)

    assert response.status_code == 400
    assert response.json() == {'error_details': {}}


@pytest.mark.parametrize('body, err_resp', [
    ({}, {'email': ['Поле не может быть пустым'], 'password': ['Поле не может быть пустым']}),
    ({'password': ''}, {'email': ['Поле не может быть пустым']}),
    ({'email': ''}, {'email': ['Email введен неправильно'], 'password': ['Поле не может быть пустым']}),
    ({'email': '', 'password': ''}, {'email': ['Email введен неправильно']}),
    ({'email': '', 'password': 'p'}, {'email': ['Email введен неправильно']}),
    ({'password': 'pass'}, {'email': ['Поле не может быть пустым']}),
])
def test_empty_fields(client, body, err_resp):
    response = client.post(PATH, json=body)

    assert response.status_code == 400
    assert response.json() == {'error_details': err_resp}
