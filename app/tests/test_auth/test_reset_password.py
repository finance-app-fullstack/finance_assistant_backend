from datetime import datetime, timedelta

import pytest

from app.tests.factories import reset_pin_code_factory

PATH = '/user/reset_password/'


def test_reset_ok(client, db, user):
    new_password = 'new_pass'
    pin = reset_pin_code_factory(db, user_id=user.id)
    data = {'email': user.email, 'password': new_password, 'password_confirm': new_password, 'pin': pin.pin}
    old_token = user.token

    response = client.post(PATH, json=data)
    db.refresh(user)

    assert response.status_code == 200
    assert user.verify_password(new_password)
    assert response.json() == {'token': user.token}
    assert not old_token == user.token


def test_reset_in_time_limit(client, db, user):
    new_password = 'new_pass'
    pin = reset_pin_code_factory(db, user_id=user.id, created=datetime.now() - timedelta(minutes=4))
    data = {'email': user.email, 'password': new_password, 'password_confirm': new_password, 'pin': pin.pin}

    response = client.post(PATH, json=data)
    db.refresh(user)

    assert response.status_code == 200
    assert response.json() == {'token': user.token}
    assert user.verify_password(new_password)


def test_reset_out_time_limit(client, db, user):
    new_password = 'new_pass'
    pin = reset_pin_code_factory(db, user_id=user.id, created=datetime.now() - timedelta(minutes=10))
    data = {'email': user.email, 'password': new_password, 'password_confirm': new_password, 'pin': pin.pin}

    response = client.post(PATH, json=data)
    db.refresh(user)

    assert response.status_code == 400
    assert user.verify_password('password')
    assert response.json() == {'error_detail': 'Не правильный pin код.'}


def test_user_not_exist(client, db):
    new_password = 'new_pass'
    data = {'email': 'mail@mail.com', 'password': new_password, 'password_confirm': new_password, 'pin': 1000}

    response = client.post(PATH, json=data)

    assert response.status_code == 400
    assert response.json() == {'error_detail': 'Не удалось найти пользователя.'}


def test_wrong_pin(client, db, user):
    new_password = 'new_pass'
    reset_pin_code_factory(db, user_id=user.id, pin=5555)
    data = {'email': user.email, 'password': new_password, 'password_confirm': new_password, 'pin': 6666}

    response = client.post(PATH, json=data)
    db.refresh(user)

    assert response.status_code == 400
    assert user.verify_password('password')
    assert response.json() == {'error_detail': 'Не правильный pin код.'}


@pytest.mark.parametrize('email', [
    '',
    'user@',
    '@email.com',
    'user@email#email.com',
    'user@^tt.com',
    'user@user'
])
def test_email_validation(client, email):
    data = {'email': email, 'password': '123', 'password_confirm': '123', 'pin': 6666}
    response = client.post(PATH, json=data)

    assert response.status_code == 400
    assert response.json() == {'error_details': {'email': ['Email введен неправильно']}}


def test_empty_body(client):
    response = client.post(PATH, json={})

    assert response.status_code == 400
    assert response.json() == {
        'error_details': {
            'email': ['Поле не может быть пустым'],
            'password': ['Поле не может быть пустым'],
            'password_confirm': ['Поле не может быть пустым'],
            'pin': ['Поле не может быть пустым']
        }
    }


@pytest.mark.parametrize('body', [
    {'password': '', 'password_confirm': '123'},
    {'password_confirm': '', 'password': '123'},
])
def test_length_password_fields(client, body):
    body.update({'email': 'mail@mail.com', 'pin': 1000})

    err = {}
    if len(body['password']) == 0:
        err = {'password': ['Не может быть меньше 1 символов']}

    if len(body['password_confirm']) == 0:
        err = {'password_confirm': ['Не может быть меньше 1 символов']}

    response = client.post(PATH, json=body)

    assert response.status_code == 400
    assert response.json() == {'error_details': err}
