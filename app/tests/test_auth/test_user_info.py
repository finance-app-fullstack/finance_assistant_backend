PATH = '/user/info/'


def test_success(client, user):
    response = client.get(PATH, headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 200
    assert response.json()['email'] == user.email


def test_invalid_token(client, db):
    response = client.get(PATH, headers={'Authorization': f'Token 123'})

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}


def test_without_auth_header(client):
    response = client.get(PATH)

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}
