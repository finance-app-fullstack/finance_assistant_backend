import pytest

from app.models import User

PATH = '/user/registration/'


def test_success(client, db):
    mail = 'user@user.com'
    password = 'password'
    data = {'email': mail, 'password': password, 'password_confirm': password}

    response = client.post(PATH, json=data)
    user = db.query(User).filter(User.email == mail).first()

    assert response.status_code == 201
    assert response.json() == {'token': user.token}
    assert user.verify_password(password)


def test_not_unique_password(client, user):
    data = {'email': user.email, 'password': 'password', 'password_confirm': 'password'}

    response = client.post(PATH, json=data)
    assert response.status_code == 400
    assert response.json() == {'error_detail': 'Пользователь с таким email уже существует'}


def test_confirmed_password_not_equal_password(client):
    data = {'email': 'user@user.com', 'password': 'password', 'password_confirm': 'password1'}

    response = client.post(PATH, json=data)
    assert response.status_code == 400
    assert response.json() == {
        'error_details': {'__root__': ['Пароли не совпадают']},
    }


@pytest.mark.parametrize('email', [
    'user@',
    '@email.com',
    'user@email#email.com',
    'user@^tt.com',
    'user@user'
])
def test_invalid_email(client, email):
    data = {'email': email, 'password': 'password', 'password_confirm': 'password'}

    response = client.post(PATH, json=data)

    assert response.status_code == 400
    assert response.json() == {'error_details': {'email': ['Email введен неправильно']}}


@pytest.mark.parametrize('body', [
    {},
    {'password': 'pass'},
    {'password_confirm': 'pass'},
    {'email': 'mail@mail.com'},
    {'email': 'mail@mail.com', 'password': 'pass'},
    {'password_confirm': 'pass', 'password': 'pass'},
])
def test_required_fields(client, body):
    required_fields = {'email', 'password', 'password_confirm'}
    fields_in_body = set(body.keys())
    invalid_fields = required_fields.difference(fields_in_body)
    errors = {}
    for invalid_field in invalid_fields:
        errors[invalid_field] = ['Поле не может быть пустым']

    response = client.post(PATH, json=body)

    assert response.status_code == 400
    assert response.json() == {'error_details': errors}


@pytest.mark.parametrize('body', [
    {'password': '', 'password_confirm': '123'},
    {'password_confirm': '', 'password': '123'},
])
def test_length_password_fields(client, body):
    body.update({'email': 'mail@mail.com'})

    err = {}
    if len(body['password']) == 0:
        err = {'password': ['Не может быть меньше 1 символов']}

    if len(body['password_confirm']) == 0:
        err = {'password_confirm': ['Не может быть меньше 1 символов']}

    response = client.post(PATH, json=body)

    assert response.status_code == 400
    assert response.json() == {'error_details': err}
