from datetime import datetime, timedelta

import pytest

from app.models import ResetPasswordPin
from app.tests.factories import reset_pin_code_factory

PATH = '/user/reset_password/generate_pin/'


def test_success_generate_pin_code(client, db, user):
    data = {'email': user.email}

    response = client.post(PATH, json=data)
    pin_code = db.query(ResetPasswordPin).filter(ResetPasswordPin.user_id == user.id)

    assert response.status_code == 201
    assert pin_code.count() == 1


def test_pin_exist(client, db, user):
    data = {'email': user.email}

    reset_pin_code_factory(db, user_id=user.id, created=datetime.now() - timedelta(minutes=10))

    response = client.post(PATH, json=data)

    assert response.status_code == 201
    assert db.query(ResetPasswordPin).filter(ResetPasswordPin.user_id == user.id).count() == 2


def test_pin_limit(client, db, user):
    data = {'email': user.email}

    reset_pin_code_factory(db, user_id=user.id)

    response = client.post(PATH, json=data)

    assert response.status_code == 400
    assert response.json() == {'error_detail': 'Превышен лимит создания pin кодов.'}
    assert db.query(ResetPasswordPin).filter(ResetPasswordPin.user_id == user.id).count() == 1


def test_user_not_exist(client, db):
    data = {'email': 'mail@mail.com'}

    response = client.post(PATH, json=data)

    assert response.status_code == 400
    assert response.json() == {'error_detail': 'Не удалось найти пользователя.'}
    assert db.query(ResetPasswordPin).count() == 0


@pytest.mark.parametrize('email', [
    '',
    'user@',
    '@email.com',
    'user@email#email.com',
    'user@^tt.com',
    'user@user'
])
def test_email_validation(client, email):
    data = {'email': email}
    response = client.post(PATH, json=data)

    assert response.status_code == 400
    assert response.json() == {'error_details': {'email': ['Email введен неправильно']}}


def test_empty_body(client):
    data = {}
    response = client.post(PATH, json=data)

    assert response.status_code == 400
    assert response.json() == {'error_details': {'email': ['Поле не может быть пустым']}}
