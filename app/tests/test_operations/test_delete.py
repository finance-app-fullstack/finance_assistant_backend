from app.tests.factories import category_factory, operation_factory, user_factory

PATH = '/operations/{}/'


def test_ok(client, db, user):
    category = category_factory(db, user, 'Категория')
    operation = operation_factory(db, category)

    response = client.delete(PATH.format(operation.id), headers={'Authorization': f'Token {user.token}'})
    db.refresh(operation)

    assert response.status_code == 204
    assert operation.deleted


def test_only_auth_user_operation(client, db, user):
    category = category_factory(db, user, 'Категория')
    operation = operation_factory(db, category)

    other_user = user_factory(db, 'e@e.com', 'pass')

    response = client.delete(PATH.format(operation.id), headers={'Authorization': f'Token {other_user.token}'})
    db.refresh(operation)

    assert response.status_code == 400
    assert response.json() == {'error_detail': 'Операция не найдена.'}
    assert not operation.deleted


def test_only_active_operation(client, db, user):
    category = category_factory(db, user, 'Категория')
    operation = operation_factory(db, category, deleted=True)

    response = client.delete(PATH.format(operation.id), headers={'Authorization': f'Token {user.token}'})
    db.refresh(operation)

    assert response.status_code == 400
    assert response.json() == {'error_detail': 'Операция не найдена.'}
    assert operation.deleted


def test_operation_not_exist(client, user):
    response = client.delete(PATH.format('123'), headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 400
    assert response.json() == {'error_detail': 'Операция не найдена.'}


def test_user_not_exists(client, db):
    response = client.delete(PATH.format('123'), headers={'Authorization': f'Token 123'})

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}


def test_without_token(client):
    response = client.delete(PATH.format('123'), headers={'Authorization': ''})

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}
