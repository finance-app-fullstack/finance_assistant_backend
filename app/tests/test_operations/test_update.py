import pytest

from app.tests.factories import category_factory, operation_factory, user_factory

PATH = '/operations/{}/'


def test_ok(client, db, user):
    category = category_factory(db, user)
    operation = operation_factory(db, category, amount=1000)

    data = {
        'name': 'Операция изменена',
        'category_id': category.id,
        'amount': 1500,
        'created': operation.created.isoformat()
    }

    response = client.put(
        PATH.format(operation.id),
        headers={'Authorization': f'Token {user.token}'},
        json=data
    )

    assert response.status_code == 200
    assert response.json() == {
        'amount': 1500,
        'category_id': operation.category_id,
        'created': operation.created.isoformat(),
        'name': 'Операция изменена'
    }


@pytest.mark.parametrize('name, error', [
    ('', 'Не может быть меньше 1 символов'),
    ('f'*201, 'Не может быть больше 200 символов')
])
def test_validate_name(name, error, db, client, user):
    category = category_factory(db, user)
    operation = operation_factory(db, category)

    data = {
        'name': name,
        'category_id': category.id,
        'amount': operation.amount,
        'created': operation.created.isoformat()
    }

    response = client.put(
        PATH.format(operation.id),
        headers={'Authorization': f'Token {user.token}'},
        json=data
    )

    assert response.status_code == 400
    assert response.json() == {'error_details': {'name': [error]}}


@pytest.mark.parametrize('amount, error', [
    (0, 'Не может быть меньше 1'),
    ('bla bla', 'Можно указать только число')
])
def test_validate_amount(amount, error, db, client, user):
    category = category_factory(db, user)
    operation = operation_factory(db, category)

    data = {
        'name': operation.name,
        'category_id': category.id,
        'amount': amount,
        'created': operation.created.isoformat()
    }

    response = client.put(
        PATH.format(operation.id),
        headers={'Authorization': f'Token {user.token}'},
        json=data
    )

    assert response.status_code == 400
    assert response.json() == {'error_details': {'amount': [error]}}


@pytest.mark.parametrize('created, error', [
    ('аааа', 'Неправильный формат времени'),
    ('15.09.2000', 'Неправильный формат времени'),
])
def test_validate_created(created, error, db, client, user):
    category = category_factory(db, user)
    operation = operation_factory(db, category)

    data = {
        'name': operation.name,
        'category_id': category.id,
        'amount': operation.amount,
        'created': created
    }

    response = client.put(
        PATH.format(operation.id),
        headers={'Authorization': f'Token {user.token}'},
        json=data
    )

    assert response.status_code == 400
    assert response.json() == {'error_details': {'created': [error]}}


@pytest.mark.parametrize('category_id, error', [
    ('аааа', 'Можно указать только число'),
    ('k2344', 'Можно указать только число'),
    ('/////', 'Можно указать только число'),
])
def test_validate_category_id(db, user, client, category_id, error):
    category = category_factory(db, user)
    operation = operation_factory(db, category)

    data = {
        'name': operation.name,
        'category_id': category_id,
        'amount': operation.amount,
        'created': operation.created.isoformat()
    }

    response = client.put(
        PATH.format(operation.id),
        headers={'Authorization': f'Token {user.token}'},
        json=data
    )

    assert response.status_code == 400
    assert response.json() == {'error_details': {'category_id': ['Можно указать только число']}}


def test_category_not_exist(db, user, client):
    category = category_factory(db, user)
    operation = operation_factory(db, category)

    data = {
        'name': operation.name,
        'category_id': category.id + 1,
        'amount': operation.amount,
        'created': operation.created.isoformat()
    }

    response = client.put(
        PATH.format(operation.id),
        headers={'Authorization': f'Token {user.token}'},
        json=data
    )

    assert response.status_code == 400
    assert response.json() == {'error_detail': 'Категория не найдена.'}


def test_empty_body(db, client, user):
    category = category_factory(db, user)
    operation = operation_factory(db, category)

    data = {
        'name': '',
        'category_id': '',
        'amount': '',
        'created': ''
    }

    response = client.put(
        PATH.format(operation.id),
        headers={'Authorization': f'Token {user.token}'},
        json=data
    )

    assert response.status_code == 400
    assert response.json() == {
        'error_details': {
            'amount': ['Можно указать только число'],
            'category_id': ['Можно указать только число'],
            'created': ['Неправильный формат времени'],
            'name': ['Не может быть меньше 1 символов']
        }
    }


def test_update_other_user(db, client, user):
    category = category_factory(db, user)
    operation = operation_factory(db, category)

    other_user = user_factory(db, 'g@g.com', 'pass')

    data = {
        'name': operation.name,
        'category_id': operation.category_id,
        'amount': operation.amount,
        'created': operation.created.isoformat()
    }

    response = client.put(
        PATH.format(operation.id),
        headers={'Authorization': f'Token {other_user.token}'},
        json=data
    )

    assert response.status_code == 400
    assert response.json() == {'error_detail': 'Операция не найдена.'}


def test_user_not_exist(db, user, client):
    category = category_factory(db, user)
    operation = operation_factory(db, category)

    data = {
        'name': operation.name,
        'category_id': operation.category_id,
        'amount': operation.amount,
        'created': operation.created.isoformat()
    }

    response = client.put(
        PATH.format(operation.id),
        headers={'Authorization': 'Token 123'},
        json=data
    )

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}


def test_without_auth_header(db, user, client):
    category = category_factory(db, user)
    operation = operation_factory(db, category)

    data = {
        'name': operation.name,
        'category_id': operation.category_id,
        'amount': operation.amount,
        'created': operation.created.isoformat()
    }

    response = client.put(
        PATH.format(operation.id),
        json=data
    )

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}
