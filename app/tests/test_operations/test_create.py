from app.tests.factories import category_factory

PATH = '/operations/'


def test_ok(client, user, db):
    category = category_factory(db, user, 'Категория')

    response = client.post(
        PATH,
        headers={'Authorization': f'Token {user.token}'},
        json={'name': 'Операция', 'category_id': category.id, 'amount': 500}
    )

    assert response.status_code == 201

    operation_response = response.json()

    assert len(operation_response.keys()) == 4
    assert operation_response['name'] == 'Операция'
    assert operation_response['category_id'] == category.id
    assert operation_response['amount'] == 500
    assert 'created' in operation_response


def test_category_deleted(client, user, db):
    category = category_factory(db, user, 'Категория', deleted=True)

    response = client.post(
        PATH,
        headers={'Authorization': f'Token {user.token}'},
        json={'name': 'Операция', 'category_id': category.id, 'amount': 500}
    )

    assert response.status_code == 400

    assert response.json() == {'error_detail': 'Категория не найдена.'}


def test_category_not_exists(client, user):
    response = client.post(
        PATH,
        headers={'Authorization': f'Token {user.token}'},
        json={'name': 'Операция', 'category_id': '123', 'amount': 500}
    )

    assert response.status_code == 400

    assert response.json() == {'error_detail': 'Категория не найдена.'}


def test_min_length_name(client, user, db):
    category = category_factory(db, user, 'Категория')
    response = client.post(
        PATH,
        headers={'Authorization': f'Token {user.token}'},
        json={'name': '', 'category_id': f'{category.id}', 'amount': 500}
    )

    assert response.status_code == 400
    assert response.json() == {'error_details': {'name': ['Не может быть меньше 1 символов']}}


def test_max_length_name(client, db, user):
    category = category_factory(db, user, 'Категория')
    response = client.post(
        PATH,
        headers={'Authorization': f'Token {user.token}'},
        json={'name': 'q' * 250, 'category_id': f'{category.id}', 'amount': 500}
    )

    assert response.status_code == 400
    assert response.json() == {'error_details': {'name': ['Не может быть больше 200 символов']}}


def test_amount_not_int(client, db, user):
    category = category_factory(db, user, 'Категория')
    response = client.post(
        PATH,
        headers={'Authorization': f'Token {user.token}'},
        json={'name': 'Операция', 'category_id': f'{category.id}', 'amount': 'fff'}
    )

    assert response.status_code == 400
    assert response.json() == {'error_details': {'amount': ['Можно указать только число']}}


def test_min_amount(client, db, user):
    category = category_factory(db, user, 'Категория')
    response = client.post(
        PATH,
        headers={'Authorization': f'Token {user.token}'},
        json={'name': 'Операция', 'category_id': f'{category.id}', 'amount': 0}
    )

    assert response.status_code == 400
    assert response.json() == {'error_details': {'amount': ['Не может быть меньше 1']}}


def test_empty_body(client, user):
    response = client.post(
        PATH,
        headers={'Authorization': f'Token {user.token}'},
        json={}
    )

    assert response.status_code == 400
    assert response.json() == {'error_details': {
        'amount': ['Поле не может быть пустым'],
        'category_id': ['Поле не может быть пустым'],
        'name': ['Поле не может быть пустым']
    }}


def test_user_not_exist(client, db):
    response = client.post(
        PATH,
        headers={'Authorization': f'Token 123'},
        json={}
    )

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}


def test_user_without_token(client):
    response = client.post(
        PATH,
        json={}
    )

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}
