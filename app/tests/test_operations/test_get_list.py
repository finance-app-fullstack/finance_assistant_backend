import datetime

import pytest

from app.tests.factories import category_factory, operation_factory, user_factory

PATH = '/operations/'


def test_ok(db, user, client):
    category_one = category_factory(db, user, 'Категория 1')
    category_two = category_factory(db, user, 'Категория 2')

    operation_one = operation_factory(db, category_one, amount=500)
    operation_two = operation_factory(db, category_one, amount=1500)
    operation_three = operation_factory(db, category_two, amount=3000)
    operation_four = operation_factory(db, category_two, amount=800)

    response = client.get(PATH, headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 200

    operations = response.json()

    assert len(operations) == 4
    assert operations[0] == {
        'id': operation_four.id,
        'name': operation_four.name,
        'amount': operation_four.amount,
        'created': operation_four.created.isoformat(),
        'category': {
            'name': operation_four.category.name,
            'id': operation_four.category_id
        }
    }

    assert operations[1] == {
        'id': operation_three.id,
        'name': operation_three.name,
        'amount': operation_three.amount,
        'created': operation_three.created.isoformat(),
        'category': {
            'name': operation_three.category.name,
            'id': operation_three.category_id
        }
    }

    assert operations[2] == {
        'id': operation_two.id,
        'name': operation_two.name,
        'amount': operation_two.amount,
        'created': operation_two.created.isoformat(),
        'category': {
            'name': operation_two.category.name,
            'id': operation_two.category_id
        }
    }

    assert operations[3] == {
        'id': operation_one.id,
        'name': operation_one.name,
        'amount': operation_one.amount,
        'created': operation_one.created.isoformat(),
        'category': {
            'name': operation_one.category.name,
            'id': operation_one.category_id
        }
    }


def test_pagination(client, db, user):
    category_one = category_factory(db, user, 'Категория 1')
    category_two = category_factory(db, user, 'Категория 2')

    operation_one = operation_factory(db, category_one, amount=500)
    operation_two = operation_factory(db, category_one, amount=1500)
    operation_factory(db, category_two, amount=3000)
    operation_factory(db, category_two, amount=800)

    response = client.get(PATH + '?limit=2&offset=2', headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 200

    operations = response.json()
    assert len(operations) == 2
    assert operations[0] == {
        'id': operation_two.id,
        'name': operation_two.name,
        'amount': operation_two.amount,
        'created': operation_two.created.isoformat(),
        'category': {
            'name': operation_two.category.name,
            'id': operation_two.category_id
        }
    }

    assert operations[1] == {
        'id': operation_one.id,
        'name': operation_one.name,
        'amount': operation_one.amount,
        'created': operation_one.created.isoformat(),
        'category': {
            'name': operation_one.category.name,
            'id': operation_one.category_id
        }
    }


@pytest.mark.parametrize('values', [
    ('a', 'b'),
    ('', ''),
])
def test_not_valid_pagination(client, user, values):
    response = client.get(PATH + f'?limit={values[0]}&offset={values[1]}',
                          headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 400
    assert response.json() == {
        'error_details': {
            'offset': ['Можно указать только число'],
            'limit': ['Можно указать только число']
        }
    }


def test_pagination_empty_pages(client, db, user):
    response = client.get(PATH, headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 200
    assert response.json() == []


def test_without_operations(client, db, user):
    response = client.get(PATH + '?limit=2&offset=2', headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 200
    assert response.json() == []


def test_filter_by_category(client, db, user):
    category_one = category_factory(db, user, 'Категория 1')
    category_two = category_factory(db, user, 'Категория 2')

    operation_one = operation_factory(db, category_one, amount=500)
    operation_two = operation_factory(db, category_one, amount=1500)
    operation_factory(db, category_two, amount=3000)
    operation_factory(db, category_two, amount=800)

    response = client.get(PATH + f'?category={category_one.id}', headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 200

    operations = response.json()

    assert len(operations) == 2
    assert operations[0]['id'] == operation_two.id
    assert operations[1]['id'] == operation_one.id


def test_filter_by_categories(client, db, user):
    category_one = category_factory(db, user, 'Категория 1')
    category_two = category_factory(db, user, 'Категория 2')
    category_three = category_factory(db, user, 'Категория 3')

    operation_one = operation_factory(db, category_one, amount=500)
    operation_two = operation_factory(db, category_two, amount=1500)
    operation_factory(db, category_three, amount=800)

    response = client.get(
        PATH + f'?category={category_one.id}&category={category_two.id}',
        headers={'Authorization': f'Token {user.token}'}
    )

    assert response.status_code == 200

    operations = response.json()

    assert len(operations) == 2
    assert operations[0]['id'] == operation_two.id
    assert operations[1]['id'] == operation_one.id


@pytest.mark.parametrize('value', [
    'a',
    -10,
])
def test_filter_by_category_invalid_value(client, db, user, value):
    category_one = category_factory(db, user, 'Категория 1')

    operation_factory(db, category_one, amount=500)
    operation_factory(db, category_one, amount=800)

    response = client.get(
        PATH + f'?category={value}',
        headers={'Authorization': f'Token {user.token}'}
    )
    if isinstance(value, int):
        assert response.status_code == 200
        assert response.json() == []
    else:
        assert response.status_code == 400
        assert response.json() == {'error_details': {'category': ['Можно указать только число']}}


def test_filter_by_date_range(client, db, user):
    category = category_factory(db, user, 'Категория 1')

    n = datetime.datetime.now()
    operation_one = operation_factory(db, category, amount=500, created=n - datetime.timedelta(days=4))
    operation_two = operation_factory(db, category, amount=1500, created=n - datetime.timedelta(days=3))
    operation_factory(db, category, amount=800, created=n - datetime.timedelta(days=2))

    start = (n - datetime.timedelta(days=4)).date()
    end = (n - datetime.timedelta(days=3)).date()
    response = client.get(
        PATH + f'?start={start.isoformat()}&end={end.isoformat()}',
        headers={'Authorization': f'Token {user.token}'}
    )

    assert response.status_code == 200

    operations = response.json()

    assert len(operations) == 2
    assert operations[0]['id'] == operation_two.id
    assert operations[1]['id'] == operation_one.id


def test_filter_by_date_range_only_one_param(client, db, user):
    category = category_factory(db, user, 'Категория 1')

    n = datetime.datetime.now()
    operation_factory(db, category, amount=500, created=n - datetime.timedelta(days=4))
    operation_factory(db, category, amount=1500, created=n - datetime.timedelta(days=3))
    operation_factory(db, category, amount=800, created=n - datetime.timedelta(days=2))

    start = (n - datetime.timedelta(days=4)).date()
    end = (n - datetime.timedelta(days=3)).date()

    response = client.get(
        PATH + f'?start={start.isoformat()}',
        headers={'Authorization': f'Token {user.token}'}
    )
    assert response.status_code == 200
    assert len(response.json()) == 3

    response = client.get(
        PATH + f'?end={end.isoformat()}',
        headers={'Authorization': f'Token {user.token}'}
    )
    assert response.status_code == 200
    assert len(response.json()) == 3


def test_filter_only_active_operations(db, user, client):
    category_one = category_factory(db, user, 'Категория 1')

    operation_factory(db, category_one, amount=500, deleted=True)

    response = client.get(
        PATH + f'?category={category_one.id}',
        headers={'Authorization': f'Token {user.token}'}
    )

    assert response.status_code == 200
    assert len(response.json()) == 0


def test_only_auth_user_operations(client, db, user):
    category = category_factory(db, user, 'Категория 1')

    operation_factory(db, category, amount=500)
    operation_factory(db, category, amount=1500)

    other_user = user_factory(db, 'e@e.com', 'pass')
    response = client.get(PATH, headers={'Authorization': f'Token {other_user.token}'})

    assert response.status_code == 200
    assert response.json() == []


def test_only_active_operations(client, db, user):
    category = category_factory(db, user, 'Категория 1')

    operation_factory(db, category, amount=500, deleted=True)
    operation_factory(db, category, amount=1500, deleted=True)

    response = client.get(PATH, headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 200
    assert response.json() == []


def test_user_not_exists(client, db):
    response = client.get(PATH, headers={'Authorization': f'Token 123'})

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}


def test_without_token(client):
    response = client.get(PATH, headers={'Authorization': ''})

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}
