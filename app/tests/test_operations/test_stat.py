import datetime

from app.tests.factories import category_factory, operation_factory, user_factory

PATH = '/operations/stat/'


def test_ok(db, user, client):
    category_one = category_factory(db, user, 'Категория 1')
    category_two = category_factory(db, user, 'Категория 2')

    operation_one = operation_factory(db, category_one, amount=500)
    operation_two = operation_factory(db, category_one, amount=1500)
    operation_three = operation_factory(db, category_two, amount=3000)
    operation_four = operation_factory(db, category_two, amount=800)

    today = datetime.datetime.now().date()
    response = client.get(
        PATH + f'?start={today.isoformat()}&end={today.isoformat()}',
        headers={'Authorization': f'Token {user.token}'}
    )

    assert response.status_code == 200

    stat = response.json()

    assert len(stat) == 2
    assert stat[0] == {
        'category_name': category_two.name,
        'total': operation_three.amount + operation_four.amount,
    }
    assert stat[1] == {
        'category_name': category_one.name,
        'total': operation_one.amount + operation_two.amount,
    }


def test_no_operation(db, user, client):
    category_one = category_factory(db, user, 'Категория 1')
    category_two = category_factory(db, user, 'Категория 2')

    today = datetime.datetime.now().date()
    response = client.get(
        PATH + f'?start={today.isoformat()}&end={today.isoformat()}',
        headers={'Authorization': f'Token {user.token}'}
    )

    assert response.status_code == 200

    stat = response.json()

    assert len(stat) == 2
    assert stat[0] == {
        'category_name': category_two.name,
        'total': 0,
    }
    assert stat[1] == {
        'category_name': category_one.name,
        'total': 0,
    }


def test_without_categories(db, user, client):
    today = datetime.datetime.now().date()
    response = client.get(
        PATH + f'?start={today.isoformat()}&end={today.isoformat()}',
        headers={'Authorization': f'Token {user.token}'}
    )

    assert response.status_code == 200
    assert response.json() == []


def test_only_auth_user_stat(db, user, client):
    other_user = user_factory(db, 'p@p.com', '123')
    category_one = category_factory(db, user, 'Категория 1')
    category_two = category_factory(db, user, 'Категория 2')

    operation_factory(db, category_one, amount=500)
    operation_factory(db, category_one, amount=1500)
    operation_factory(db, category_two, amount=3000)
    operation_factory(db, category_two, amount=800)

    today = datetime.datetime.now().date()
    response = client.get(
        PATH + f'?start={today.isoformat()}&end={today.isoformat()}',
        headers={'Authorization': f'Token {other_user.token}'}
    )

    assert response.status_code == 200
    assert response.json() == []


def test_without_dates(db, user, client):
    category_one = category_factory(db, user, 'Категория 1')
    category_two = category_factory(db, user, 'Категория 2')

    operation_factory(db, category_one, amount=500)
    operation_factory(db, category_one, amount=1500)
    operation_factory(db, category_two, amount=3000)
    operation_factory(db, category_two, amount=800)

    response = client.get(PATH, headers={'Authorization': f'Token {user.token}'})

    assert response.status_code == 400

    assert response.json() == {
        'error_details': {
            'end': ['Поле не может быть пустым'],
            'start': ['Поле не может быть пустым']
        }
    }


def test_invalid_dates(db, user, client):
    category_one = category_factory(db, user, 'Категория 1')
    category_two = category_factory(db, user, 'Категория 2')

    operation_factory(db, category_one, amount=500)
    operation_factory(db, category_one, amount=1500)
    operation_factory(db, category_two, amount=3000)
    operation_factory(db, category_two, amount=800)

    response = client.get(
        PATH + f'?start=df&end=gh',
        headers={'Authorization': f'Token {user.token}'}
    )

    assert response.status_code == 400
    assert response.json() == {
        'error_details': {
            'start': ['Неправильный формат времени'],
            'end': ['Неправильный формат времени']
        }
    }


def test_user_not_exists(client, db):
    response = client.get(PATH, headers={'Authorization': f'Token 123'})

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}


def test_without_token(client):
    response = client.get(PATH, headers={'Authorization': ''})

    assert response.status_code == 403
    assert response.json() == {'detail': 'Ошибка авторизации'}