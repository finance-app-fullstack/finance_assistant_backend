from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError

from .models import Base
from .database import engine
from .exceptions import CRUDException, handle_crud_exception, handle_validation_error
from .auth.router import router as auth_router
from .categories.router import router as categories_router
from .operations.router import router as operations_router


Base.metadata.create_all(bind=engine)

app = FastAPI()

app.add_exception_handler(CRUDException, handle_crud_exception)
app.add_exception_handler(RequestValidationError, handle_validation_error)


app.include_router(auth_router, prefix="/user")
app.include_router(categories_router, prefix="/categories")
app.include_router(operations_router, prefix="/operations")
