import binascii
import os

from passlib.handlers.pbkdf2 import pbkdf2_sha256
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, DateTime, func
from sqlalchemy.orm import relationship

from app.database import Base


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)
    email = Column(String(254), unique=True)
    token = Column(String(40), unique=True)
    hashed_password = Column(String(87))

    categories = relationship('OperationCategory', back_populates='user')
    operations = relationship('Operation', back_populates='user')
    pin_codes = relationship('ResetPasswordPin', lazy='dynamic')

    def verify_password(self, password):
        return pbkdf2_sha256.verify(password, self.hashed_password)

    @staticmethod
    def get_hash_password(password):
        return pbkdf2_sha256.hash(password)

    @staticmethod
    def generate_token():
        return binascii.hexlify(os.urandom(20)).decode()


class ResetPasswordPin(Base):
    __tablename__ = "reset_password_pin"

    id = Column(Integer, primary_key=True)
    pin = Column(Integer)
    created = Column(DateTime(timezone=True), server_default=func.now())
    user_id = Column(Integer, ForeignKey('user.id'))


class OperationCategory(Base):
    __tablename__ = 'operation_category'

    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    deleted = Column(Boolean, default=False)
    created = Column(DateTime(timezone=True), server_default=func.now())

    user_id = Column(Integer, ForeignKey('user.id'))

    user = relationship(User, back_populates='categories')
    operations = relationship('Operation', back_populates='category', lazy='dynamic')


class Operation(Base):
    __tablename__ = 'operation'

    id = Column(Integer, primary_key=True)
    name = Column(String(200))
    amount = Column(Integer)
    deleted = Column(Boolean, default=False)
    created = Column(DateTime(timezone=True), server_default=func.now())

    category_id = Column(Integer, ForeignKey('operation_category.id'))
    user_id = Column(Integer, ForeignKey('user.id'))

    user = relationship(User, back_populates='operations')
    category = relationship(OperationCategory, back_populates='operations')
