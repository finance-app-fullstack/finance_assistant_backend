from typing import Optional

from pydantic import BaseSettings


class Settings(BaseSettings):
    db_link: str
    test_db_link: str = ''
    mail_debug: bool = True
    smtp_server: Optional[str]
    smtp_port: Optional[int]
    smtp_from: Optional[str]
    smtp_password: Optional[str]


settings = Settings()
