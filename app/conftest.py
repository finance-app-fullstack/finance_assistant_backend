import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from fastapi.testclient import TestClient

from .config import settings
from .dependencies import db_session
from .main import app
from .models import User, Base
from .tests.factories import user_factory


@pytest.fixture(scope='session')
def engine():
    return create_engine(settings.test_db_link)


@pytest.fixture(scope='function')
def tables(engine):
    Base.metadata.create_all(bind=engine)

    yield

    Base.metadata.drop_all(bind=engine)


@pytest.fixture(scope='function')
def db(engine, tables):
    connection = engine.connect()
    transaction = connection.begin()
    session = sessionmaker(bind=engine)()
    try:
        yield session
    finally:
        session.close()
        transaction.rollback()
        connection.close()


@pytest.fixture
def client():
    def get_db():
        """Подмена соединения"""
        engine = create_engine(settings.test_db_link)
        session = sessionmaker(bind=engine)()
        try:
            yield session
        finally:
            session.close()
    app.dependency_overrides[db_session] = get_db
    return TestClient(app)


@pytest.fixture(scope='function')
def user(db):
    db_user = user_factory(db, email='user@useremail.com', password='password')
    return db_user
