from datetime import date
from typing import List, Union

from fastapi import APIRouter, Depends, Query
from fastapi.responses import Response
from sqlalchemy.orm import Session

from app import crud
from app.dependencies import db_session, get_user
from app.models import User
from app.operations.schemas import (
    OperationChange,
    OperationDetail,
    OperationCategory,
    OperationStat,
)

router = APIRouter()


@router.get('/', response_model=List[OperationDetail])
def get_user_operations(
        db: Session = Depends(db_session),
        user: User = Depends(get_user),
        offset: int = Query(None),
        limit: int = Query(None),
        category: List[int] = Query(None),
        start: date = Query(None),
        end: date = Query(None),
):
    categories = None
    if category:
        if isinstance(category, int):
            categories = [category]
        else:
            categories = category

    date_range = (start, end) if start and end else None
    return crud.user_operations(
        db,
        user.id,
        limit=limit,
        offset=offset,
        categories=categories,
        date_range=date_range
    )


@router.post('/', status_code=201, response_model=OperationChange)
def create_operation(
        operation_data: OperationChange,
        db: Session = Depends(db_session),
        user: User = Depends(get_user)
):
    return crud.create_operation(db, user.id, operation_data)


@router.get('/stat/', response_model=List[OperationStat])
def get_operations_stat(
        start: date,
        end: date,
        db: Session = Depends(db_session),
        user: User = Depends(get_user)
):
    stat_info = dict(crud.operation_stat(db, user.id, (start, end)))
    categories = crud.get_user_categories(db, user.id)
    result = []
    for category in categories:
        result.append(
            OperationStat(category_name=category.name, total=stat_info.get(category.id, 0))
        )
    return result


@router.get('/{operation_id}/', response_model=OperationDetail)
def operation_detail(operation_id: int, db: Session = Depends(db_session), user: User = Depends(get_user)):
    operation_db = crud.user_operation(db, user.id, operation_id)
    operation = OperationDetail.from_orm(operation_db)
    operation.category = OperationCategory.from_orm(operation_db.category)
    return operation


@router.put('/{operation_id}/', response_model=OperationChange)
def operation_update(
        operation_id: int,
        operation_data: OperationChange,
        db: Session = Depends(db_session),
        user: User = Depends(get_user)
):
    updated_operation = crud.update_user_operation(db, user.id, operation_id, operation_data)
    return updated_operation


@router.delete('/{operation_id}/')
def operation_remove(operation_id: int, db: Session = Depends(db_session), user: User = Depends(get_user)):
    crud.remove_operation(db, user.id, operation_id)
    return Response(status_code=204)

