from datetime import datetime

from typing import Optional
from pydantic import BaseModel, Field


class OperationBase(BaseModel):
    name: str = Field(max_length=200, min_length=1)
    amount: int = Field(gt=1)


class OperationChange(OperationBase):
    category_id: int
    created: Optional[datetime]

    class Config:
        orm_mode = True


class OperationCategory(BaseModel):
    id: int
    name: str

    class Config:
        orm_mode = True


class OperationDetail(OperationBase):
    id: int
    created: datetime
    category: OperationCategory

    class Config:
        orm_mode = True


class OperationStat(BaseModel):
    category_name: str
    total: int
