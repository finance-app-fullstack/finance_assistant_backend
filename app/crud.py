import datetime
import random
from typing import List, Tuple

from sqlalchemy import desc, func
from sqlalchemy.orm import Session

from app.categories.schemas import CategoryCreate
from app.auth.schemas import UserRegistration, UserLogin, UserResetPassword
from .models import (
    User,
    OperationCategory,
    ResetPasswordPin,
    Operation,
)
from .exceptions import CRUDException
from .operations.schemas import OperationChange


def get_user_by_token(db: Session, token: str) -> User:
    return db.query(User).filter(User.token == token).first()


def get_user_by_id(db: Session, user_id: int) -> User:
    return db.query(User).filter(User.id == user_id).first()


def user_verification(db: Session, login_data: UserLogin) -> str:
    user = db.query(User).filter(User.email == login_data.email).first()

    if user:
        if user.verify_password(login_data.password):
            return user.token
    raise CRUDException('Неправильный email или пароль')


def refresh_user_token(db: Session, token: str):
    user = get_user_by_token(db, token)
    if user:
        user.token = User.generate_token()
        db.commit()
    else:
        raise CRUDException('Не удалось найти пользователя.')


def create_user(db: Session, user: UserRegistration) -> str:
    double_user = db.query(User).filter(User.email == user.email).first()
    if double_user:
        raise CRUDException('Пользователь с таким email уже существует')
    token = User.generate_token()
    db_user = User(
        email=user.email,
        hashed_password=User.get_hash_password(user.password),
        token=token
    )

    db.add(db_user)
    db.commit()
    return token


def change_user_password(db: Session, token: str, new_password: str) -> str:
    user = get_user_by_token(db, token)
    if user:
        user.token = User.generate_token()
        user.hashed_password = User.get_hash_password(new_password)
        db.commit()
        # db.refresh(user)
        return user.token
    else:
        raise CRUDException('Не удалось найти пользователя.')


def generate_pin_code(db: Session, email: str) -> int:
    user = db.query(User).filter(User.email == email).first()
    if user:
        time_limit = datetime.datetime.now() - datetime.timedelta(minutes=5)
        exist_pin = user.pin_codes.filter(ResetPasswordPin.created >= time_limit).first()
        if exist_pin:
            raise CRUDException('Превышен лимит создания pin кодов.')
        else:
            new_pin = random.randint(1000, 9999)

            pin = ResetPasswordPin(
                user_id=user.id,
                pin=new_pin
            )
            db.add(pin)
            db.commit()
            return new_pin
    else:
        raise CRUDException('Не удалось найти пользователя.')


def reset_password(db: Session, data: UserResetPassword) -> str:
    user = db.query(User).filter(User.email == data.email).first()

    if user:
        time_limit = datetime.datetime.now() - datetime.timedelta(minutes=5)
        exist_pin = user.pin_codes.filter(
            ResetPasswordPin.created >= time_limit,
            ResetPasswordPin.pin == data.pin,
        ).first()

        if exist_pin and exist_pin.pin == data.pin:
            token = User.generate_token()
            user.token = token
            user.hashed_password = User.get_hash_password(data.password)
            db.commit()
            return token

        raise CRUDException('Не правильный pin код.')
    raise CRUDException('Не удалось найти пользователя.')


def create_category(db: Session, category_data: CategoryCreate) -> OperationCategory:
    category = OperationCategory(**category_data.dict())

    db.add(category)
    db.commit()
    return category


def get_user_categories(db: Session, user_id: int) -> List[OperationCategory]:
    categories = db.query(OperationCategory).filter(
        OperationCategory.user_id == user_id,
        OperationCategory.deleted.is_(False)
    ).order_by(
        desc(OperationCategory.created)
    ).all()
    return categories


def get_user_category(db: Session, user_id: int, category_id: int) -> OperationCategory:
    category = db.query(OperationCategory).filter(
        OperationCategory.user_id == user_id,
        OperationCategory.id == category_id,
        OperationCategory.deleted.is_(False)
    ).first()
    if category:
        return category
    raise CRUDException('Категория не найдена.')


def remove_category(db: Session, user_id: int, category_id: int):
    category = db.query(OperationCategory).filter(
        OperationCategory.user_id == user_id,
        OperationCategory.id == category_id,
        OperationCategory.deleted.is_(False)
    ).first()
    if category:
        category.deleted = True
        category.operations.filter(
            Operation.deleted.is_(False)
        ).update({
            'deleted': True
        })
        db.commit()
    else:
        raise CRUDException('Категория не найдена.')


def update_category(db: Session, user_id: int, category_id: int, name: str) -> OperationCategory:
    category = db.query(OperationCategory).filter(
        OperationCategory.user_id == user_id,
        OperationCategory.id == category_id,
        OperationCategory.deleted.is_(False)
    ).first()
    if category:
        category.name = name
        db.commit()
        return category
    else:
        raise CRUDException('Категория не найдена.')


def create_operation(db: Session, user_id: int, operation_data: OperationChange) -> Operation:
    get_user_category(db, user_id, operation_data.category_id)
    operation = Operation(
        name=operation_data.name,
        amount=operation_data.amount,
        user_id=user_id,
        category_id=operation_data.category_id
    )

    db.add(operation)
    db.commit()
    return operation


def user_operations(
        db: Session,
        user_id: int,
        limit: int = None,
        offset: int = None,
        categories: List[int] = None,
        date_range: Tuple[datetime.date, datetime.date] = None
) -> List[Operation]:
    if limit is None:
        limit = 10
    if offset is None:
        offset = 0
    query = db.query(Operation).filter(
        Operation.user_id == user_id,
        Operation.deleted.is_(False)
    )

    if categories:
        query = query.filter(Operation.category_id.in_(categories))

    if date_range:
        start = date_range[0]
        end = date_range[1]
        start = datetime.datetime(day=start.day, month=start.month, year=start.year, hour=0, minute=0, second=0)
        end = datetime.datetime(day=end.day, month=end.month, year=end.year, hour=23, minute=59, second=59)
        query = query.filter(
            Operation.created >= start,
            Operation.created <= end,
        )

    return query.join(
        OperationCategory
    ).order_by(
        desc(Operation.created)
    ).limit(
        limit
    ).offset(
        offset
    ).all()


def user_operation(db: Session, user_id: int, operation_id: int) -> Operation:
    operation = db.query(Operation).filter(
        Operation.id == operation_id,
        Operation.user_id == user_id,
        Operation.deleted.is_(False)
    ).first()
    if operation:
        return operation
    else:
        raise CRUDException('Операция не найдена.')


def update_user_operation(db: Session, user_id: int, operation_id: int, operation_data: OperationChange):
    operation = db.query(Operation).filter(
        Operation.user_id == user_id,
        Operation.id == operation_id,
        Operation.deleted.is_(False)
    ).first()
    if operation:
        get_user_category(db, user_id, operation_data.category_id)
        for attr, value in operation_data.dict().items():
            setattr(operation, attr, value)
        db.commit()
        return operation
    else:
        raise CRUDException('Операция не найдена.')


def remove_operation(db: Session, user_id: int, operation_id: int):
    operation = db.query(Operation).filter(
        Operation.user_id == user_id,
        Operation.id == operation_id,
        Operation.deleted.is_(False)
    ).first()
    if operation:
        operation.deleted = True
        db.commit()
    else:
        raise CRUDException('Операция не найдена.')


def operation_stat(db: Session, user_id: int, date_range: Tuple[datetime.date, datetime.date]):
    start = date_range[0]
    end = date_range[1]
    start = datetime.datetime(day=start.day, month=start.month, year=start.year, hour=0, minute=0, second=0)
    end = datetime.datetime(day=end.day, month=end.month, year=end.year, hour=23, minute=59, second=59)

    query = db.query(
        Operation.category_id,
        func.sum(Operation.amount)
    ).filter(
        Operation.user_id == user_id,
        Operation.deleted.is_(False),
        Operation.created >= start,
        Operation.created <= end,
    ).group_by(
        Operation.category_id
    ).all()
    return query
