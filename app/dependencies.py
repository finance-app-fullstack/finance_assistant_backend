from fastapi import HTTPException, Depends, Security
from fastapi.security import APIKeyHeader
from sqlalchemy.orm import Session

from app import crud
from app.database import SessionLocal
from app.models import User


def db_session() -> Session:
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


api_key_header = APIKeyHeader(name='Authorization', auto_error=False)


def get_auth_token(authorization: str = Security(api_key_header)) -> str:
    if authorization:
        data = authorization.split(' ')
        if len(data) == 2 and data[0] == 'Token':
            return data[1]
    raise HTTPException(status_code=403, detail='Ошибка авторизации')


def get_user(token: str = Depends(get_auth_token), db: Session = Depends(db_session)) -> User:
    user = crud.get_user_by_token(db, token)
    if not user:
        raise HTTPException(status_code=403, detail='Ошибка авторизации')
    return user

