from fastapi import APIRouter, Depends, BackgroundTasks
from fastapi.responses import Response
from sqlalchemy.orm import Session

from app import crud
from app.dependencies import db_session, get_auth_token, get_user
from app.utils import send_mail

from .schemas import UserRegistration, UserLogin, UserBase, UserResetPassword


router = APIRouter()


@router.post('/registration/', status_code=201)
def registration_user(user_data: UserRegistration, db: Session = Depends(db_session)):
    return {'token': crud.create_user(db, user_data)}


@router.post('/login/')
def login(user_login_data: UserLogin, db: Session = Depends(db_session)):
    return {'token': crud.user_verification(db, user_login_data)}


@router.post('/reset_password/generate_pin/')
def reset_password_generate_pin(data: UserBase, background_tasks: BackgroundTasks, db: Session = Depends(db_session)):
    pin = crud.generate_pin_code(db, data.email)

    params = {
        'email': data.email,
        'subject': 'Восстановления пароля',
        'message': f'Пин код для восстановления пароля: {pin}'
    }
    background_tasks.add_task(send_mail, **params)
    return Response(status_code=201)


@router.post('/reset_password/')
def reset_password(data: UserResetPassword, db: Session = Depends(db_session)):
    return {'token': crud.reset_password(db, data)}


@router.post('/logout/')
def logout(db: Session = Depends(db_session), token: str = Depends(get_auth_token)):
    crud.refresh_user_token(db, token)
    return Response()


@router.get('/info/', response_model=UserBase)
def get_user_info(user: str = Depends(get_user)):
    return user
