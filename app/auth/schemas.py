from pydantic import BaseModel, EmailStr, root_validator, Field


class UserBase(BaseModel):
    email: EmailStr

    class Config:
        orm_mode = True


class UserRegistration(UserBase):
    password: str = Field(min_length=1)
    password_confirm: str = Field(min_length=1)

    @root_validator
    def check_passwords_match(cls, values):
        password_1, password_2 = values.get('password'), values.get('password_confirm')

        if password_1 and password_2 and password_1 != password_2:
            raise ValueError('Пароли не совпадают')
        return values


class UserLogin(UserBase):
    password: str


class UserResetPassword(UserRegistration):
    pin: int


