from collections import defaultdict

from fastapi.responses import JSONResponse

from .errors import ERRORS


class CRUDException(Exception):
    message: str

    def __init__(self, message: str):
        self.message = message


def handle_crud_exception(request, exc):
    return JSONResponse(status_code=400, content={'error_detail': exc.message})


def handle_validation_error(request, exc):
    errors = defaultdict(list)
    for err_info in exc.errors():
        try:
            field = err_info['loc'][1]
            msg = ERRORS.get(err_info['type'], err_info['msg'])

            if err_info.get('ctx'):
                msg = msg.format(**err_info['ctx'])

            errors[field].append(msg)
        except (IndexError, KeyError):
            pass
    return JSONResponse(status_code=400, content={'error_details': errors})
