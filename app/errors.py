ERRORS = {
    'value_error.email': 'Email введен неправильно',
    'value_error.missing': 'Поле не может быть пустым',
    'value_error.any_str.min_length': 'Не может быть меньше {limit_value} символов',
    'value_error.any_str.max_length': 'Не может быть больше {limit_value} символов',
    'value_error.number.not_gt': 'Не может быть меньше {limit_value}',
    'value_error.datetime': 'Неправильный формат времени',
    'value_error.date': 'Неправильный формат времени',
    'type_error.integer': 'Можно указать только число',
}
