FROM tiangolo/uvicorn-gunicorn:python3.7

COPY ./app /app/app

COPY requirements.txt /app

RUN pip install -r requirements.txt
